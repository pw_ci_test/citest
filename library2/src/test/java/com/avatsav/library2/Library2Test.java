package com.avatsav.library2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Library2Test {

    @Parameterized.Parameter
    public int expected;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0}, {1}, {2},
                {3}, {4}, {5},
                {6}, {7}, {8},
                {9}, {10}, {11}
        });
    }

    @Test
    public void testLibrary2() throws Exception {
        Thread.sleep(1000);
        System.out.println(expected);
    }
}
